from django.http import HttpResponse
from django.shortcuts import render
from .models import Restaurant

def index(request):
    restaurant = Restaurant.objects.all()
    context = {'restaurant':restaurant}
    # return render(request, 'restaurantsApp/base.html', context)
    return render(request, 'restaurantsApp/index.html', context)

def aboutMe(request):
    return render(request, 'restaurantsApp/aboutMe.html')

def practice(request):
    return render(request, 'restaurantsApp/blockPracticeIndex.html')

def test(request):
    return render(request, 'restaurantsApp/blockPracticeTest.html')