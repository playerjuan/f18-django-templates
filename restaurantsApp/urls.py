from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('practice/', views.practice, name='index'),
    path('test/', views.test, name='index'),
    path('cool/', views.aboutMe, name='index'),
]