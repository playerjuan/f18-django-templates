from django.db import models

# Create your models here.
class Restaurant(models.Model):
    name = models.CharField(max_length=100)
    type = models.CharField(max_length=100)
    address = models.CharField(max_length=300)
    rating = models.IntegerField()

    def __str__(self):
        returnString = "ID: " + str(self.id) + " - " + self.name + " is a " + self.type + " style of restaurant."
        return returnString