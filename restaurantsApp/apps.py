from django.apps import AppConfig


class RestaurantsappConfig(AppConfig):
    name = 'restaurantsApp'
